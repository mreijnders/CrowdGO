import sys
from collections import defaultdict

tp = 0
fp = 0
trueDict = defaultdict(set)
for line in open(sys.argv[1]):
	prot,go = line.strip().split('\t')
	trueDict[prot].add(go)

threshold = float(sys.argv[3])
for line in open(sys.argv[2]):
	prot,go,score = line.strip().split('\t')
	if go in trueDict[prot] and float(score) >= threshold:
		tp += 1
	elif not go in trueDict[prot] and float(score) >= threshold:
		fp += 1

print(tp,fp,tp/(tp+fp))
