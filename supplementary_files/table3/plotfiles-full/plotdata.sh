#!/bin/bash

./newEval.py /fatty/wei2go/data_old/goCounts.tab /fatty/wei2go/data_old/nameSpaces.tab /fatty/wei2go/data_old/goChildren.tab testset.mf.type1.tab test.mf.uppropagated.type1.tab CrowdGO ../goParents.tab > plotfiles/crowdgo.mf.tab
cat plotfiles/header.tab > plotfiles/all.mfo.tab
cat plotfiles/*mf.tab >> plotfiles/all.mfo.tab
python lineplot.py plotfiles/all.mfo.tab 'Molecular Function' mf.pdf
