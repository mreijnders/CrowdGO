import matplotlib
matplotlib.use('Agg')
import sys
import pandas as pd
import seaborn
import matplotlib as plt

df = pd.read_table(sys.argv[1])

seaborn.set_context('notebook')
lineplot = seaborn.lineplot(x="Recall",y="Precision",hue='Tool',palette='coolwarm',data=df)
lineplot.set(xlim=(0,1))
lineplot.set(ylim=(0,1))
lineplot.set(xlabel='Recall',ylabel='Precision',title=sys.argv[2])
lineplot.legend(loc=1)
lineplot.plot()
handles, labels = lineplot.get_legend_handles_labels()
lineplot.legend(handles=handles[1:], labels=labels[1:])
fig = lineplot.get_figure()
fig.savefig(sys.argv[3],dpi=1200)
