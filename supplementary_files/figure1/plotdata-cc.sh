#!/bin/bash

./newEval.py /fatty/wei2go/data/goCounts.tab /fatty/wei2go/data/nameSpaces.tab /fatty/wei2go/data/goChildren.tab testset.cc.uppropagated.tab test.cc.uppropagated.tab CrowdGO ../goParents.tab > plotfiles/crowdgo.cc.tab
cat plotfiles/header.tab > plotfiles/all.cco.tab
cat plotfiles/*cc.tab >> plotfiles/all.cco.tab
python lineplot.py plotfiles/all.cco.tab 'Cellular Component' cc.png
