#!/usr/bin/python3
import sys
from collections import defaultdict

altIdDict = defaultdict(list)
for line in open(sys.argv[3]):
	if line.startswith('id: GO:'):
		go = line.strip().split(' ')[1]
	elif line.startswith('alt_id: GO'):
		altGo = line.strip().split(' ')[1]
		altIdDict[go].append(altGo)
		altIdDict[altGo].append(go)

countDict = defaultdict(int)
for line in open(sys.argv[1]):
	if not line.startswith('Protein'):
			if not line.strip().startswith('GO:'):
				prot,term = line.strip().split('\t')
				countDict[term] += 1
				for altId in altIdDict[term]:
					countDict[altId] += 1

for line in open(sys.argv[2]):
	if not line.startswith('Protein'):
		if not line.strip().startswith('GO:'):
			prot,term = line.strip().split('\t')
			countDict[term] += 1
			for altId in altIdDict[term]:
				countDict[altId] += 1

for term,count in countDict.items():
	print(term+'\t'+str(count))
