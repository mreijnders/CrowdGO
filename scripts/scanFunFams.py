#!/usr/bin/env python3
import sys
import subprocess

list = []
for line in open(sys.argv[1]):
	if not line.startswith('#') and not line.strip() == '':
		ssline = line.strip().split(' ')
		list.append(ssline[1])

list = set(list)

for funfam in list:
	print(funfam)
	command = sys.argv[3]+'/apps/retrieve_FunFam_aln_GO_EC_anno_CATH-API.pl '+funfam+' v4_2_0 '+sys.argv[2]
	subprocess.call(command,shell=True)
