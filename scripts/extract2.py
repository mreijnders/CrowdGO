#!/usr/bin/env python3
import sys
import argparse

parser = argparse.ArgumentParser(description='Calculate Fmax')
parser.add_argument('-i','--input',help='Input file',required=True,action='store')
parser.add_argument('-o','--output',help='Output file',required=True,action='store')
args = parser.parse_args()
inputFilePath = args.input
outFilePath = args.output

outFile = open(outFilePath,'w')
for line in open(inputFilePath):
	ssline = line.strip().split('\t')
	if ssline[0] == 'ARGOT2.5':
		outFile.write(line)
	elif ssline[0] == 'DEEPGOPLUS':
		outFile.write(line)
outFile.close()
