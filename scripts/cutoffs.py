#!/usr/bin/env python3
import argparse

parser = argparse.ArgumentParser(description='Calculate Fmax')
parser.add_argument('-i','--input',help='Input file',required=True,action='store')
parser.add_argument('-a','--argot',help='Argot2.5 cutoff',required=True,action='store')
parser.add_argument('-d','--deepgo',help='DeepGOPlus cutoff',required=True,action='store')
parser.add_argument('-o','--output',help='Output file',required=True,action='store')
args = parser.parse_args()
inputFilePath = args.input
argotCutoff = float(args.argot)
deepgoCutoff = float(args.deepgo)
outFilePath = args.output

outFile = open(outFilePath,'w')
for line in open(inputFilePath):
	ssline = line.strip().split('\t')
	if ssline[0].upper() == 'ARGOT2.5' and float(ssline[3]) >= argotCutoff:
		outFile.write(line)
	elif ssline[0].upper() == 'DEEPGOPLUS' and float(ssline[3]) >= deepgoCutoff:
		outFile.write(line)
	elif ssline[0].upper() == 'IPRSCAN':
		outFile.write(line)
outFile.close()
