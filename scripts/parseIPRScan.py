#!/usr/bin/env python3
import sys
from collections import defaultdict

annotDict = defaultdict(set)
for line in open(sys.argv[1]):
	ssline = line.strip().split('\t')
	if ssline[-1].startswith('GO:'):
		for go in ssline[-1].split('|'):
			annotDict[ssline[0]].add(go)

for prot,goTerms in annotDict.items():
	for goTerm in goTerms:
		print(prot+'\t'+goTerm+'\t1')
