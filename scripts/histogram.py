#!/usr/bin/env python3
import matplotlib
matplotlib.use('Agg')
import sys
import numpy as np
import matplotlib.pyplot as plt

trueList = []
falseList = []
for line in open(sys.argv[1]):
	methodprot,go,score,label = line.strip().split('\t')
	if method == sys.argv[2]:
		if label == 'True':
			trueList.append(float(score))
		else:
			falseList.append(float(score))

plt.hist(trueList, bins='auto')
plt.savefig('histTrue.png')

plt.hist(falseList,bins='auto')
plt.savefig('histFalse_overlap_over_true.png')
