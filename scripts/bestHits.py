#!/usr/bin/env python3
import sys
from collections import defaultdict
import argparse

parser = argparse.ArgumentParser(description='Calculate Fmax')
parser.add_argument('-i','--input',help='Input file',required=True,action='store')
parser.add_argument('-r','--relations',help='GO relations file',required=True,action='store')
parser.add_argument('-o','--output',help='Output file',required=True,action='store')
args = parser.parse_args()
inputFilePath = args.input
goRelationsFilePath = args.relations
outputFilePath = args.output

relationsDict = defaultdict(list)
for line in open(goRelationsFilePath):
	parent,child = line.strip().split('\t')
	relationsDict[parent].append(parent)
	relationsDict[parent].append(child)
	relationsDict[child].append(parent)
	relationsDict[child].append(child)

annotationDict = defaultdict(dict)
for line in open(inputFilePath):
	prot,go,score = line.strip().split('\t')
	annotationDict[prot][go] = score

annotList = []
for prot,preds in annotationDict.items():
	bestHits = []
	for pred in preds:
		bestScore = annotationDict[prot][pred]
		bestGo = pred
		relatedTerms = relationsDict[pred]
		for pred2 in preds:
			if pred2 in relatedTerms:
				pred2Score = annotationDict[prot][pred2]
				if pred2Score > bestScore:
					bestScore = pred2Score
					bestGo = pred2
		bestHits.append([bestGo,bestScore])
	for bestHit in bestHits:
		annotList.append(prot+'\t'+bestHit[0]+'\t'+bestHit[1])

outFile = open(outputFilePath,'w')
outFile.write('Protein\tGO term\tScore\n')
for annot in set(annotList):
	if not annot.startswith('Protein'):
		outFile.write(annot+'\n')
outFile.close()
