##
## CrowdGO: a wisdom of the crowd based Gene Ontology annotation tool
## 
## Authors: MJMF Reijnders (maarten.reijnders@unil.ch) & RM Waterhouse 2020 (robert.waterhouse@unil.ch)
##

import argparse
import copy
import joblib
import numpy as np
import os
import pandas as pd
import time
from collections import defaultdict
from math import log10
from sklearn.preprocessing import StandardScaler,normalize

##
## Function to read the input arguments: -i <training.tab>, -o <output_dir>.
## Returns: file paths for option -i and option -o, and the True or False value for -b (default = False).
##

def readArguments():
	parser = argparse.ArgumentParser(description='CrowdGO protein function prediction')
	parser.add_argument('-i','--input',help='Input prediction file. Tab separated. Column 1 = prediction method, column 2 = protein ID, column 3 = GO term prediction ("GO:<number>"), column 4 = confidence interval',required=True,action='store')
	parser.add_argument('-m','--model',help='Input model file. Created using CrowdGO_training.py',required=True,action='store')
	parser.add_argument('-o','--outdir',help='Dir for storing the annotations and other produced files',required=True,action='store')
	args = parser.parse_args()
	inputFilePath = args.input
	modelFilePath = args.model
	outDirPath = args.outdir
	return(inputFilePath,modelFilePath,outDirPath)

##
## Reads the <input.tab> file, returns a dictionary with each GO annotation method as key and a list in a list containing 1 protein, 1 GO, and 1 score for each annotation.
## Returns: A dictionary containing the predictions (values) for each method (key), sorted by method in upper case.
##

def readInput(inputFilePath):
        infile = open(inputFilePath)
        predictionDictionary = defaultdict(dict)
        for line in infile.readlines():
                method,protein,go,score = line.strip().split('\t')
                method = method.upper()
                predictionDictionary[method].setdefault(protein,set([]))
                predictionDictionary[method][protein].add((go,score))
        infile.close()
        return(predictionDictionary)

##
## Function reading the data/goRelations.tab file. Returns three dictionaries: a dictionary with all the parent GO terms for a GO term, a dictionary with all the child GO terms for a go term, and a dictionary with all the parent and child terms for a GO term (goSlimDic).
## Returns: Three dictionaries of GO term (key) and related GO terms in a list (value). Parents dictionary = child + parent according to the GO DAG, child dictionary = parent + child according to the GO dag, and goSlimDic is the two combined
##

def goSlim(crowdGOPath):
    goRelationsFilePath = crowdGOPath+'/data/goRelations.tab'
    goParentsFilePath = crowdGOPath+'/data/goParents.tab'
    goChildrenFilePath = crowdGOPath+'/data/goChildren.tab'
    parentDic = defaultdict(list)
    childDic = defaultdict(list)
    goSlimDic = defaultdict(list)
    goParentsFile = open(goParentsFilePath)
    for line in goParentsFile.readlines():
        child,parent = line.strip().split('\t')
        parentDic[child].append(parent)
        goSlimDic[child].append(parent)
    goParentsFile.close()
    goChildrenFile = open(goChildrenFilePath)
    for line in goChildrenFile.readlines():
        parent,child = line.strip().split('\t')
        childDic[parent].append(child)
        goSlimDic[parent].append(child)
    return goSlimDic,parentDic,childDic

##
## Function reading the data/goCounts.tab file. Returns a dictionary containing the amount of times a GO term is assigned to a protein in the UniProt database (via goCounts.tab), and an integer containing the total amount of GO terms assigned to a protein in the UniProt database.
## Returns: Dictionary of GO counts in the UniProt database, and the total number of GO terms
##

def getGoCounts(crowdGOPath):
	totalGoCounts = 0
	goCountDic = defaultdict(int)
	infile = open(crowdGOPath+'/data/goCounts.tab')
	for line in infile.readlines():
		go,count = line.strip().split('\t')
		if not go.startswith('GO:'):
			go = 'GO:'+go
		goCountDic[go] = int(count)
		totalGoCounts += int(count)
	return(goCountDic,totalGoCounts)

##
## Function to retrieve all the name spaces for a GO term: biological process, molecular function, or cellular component. Returns a dictionary with as key the GO term and value the name space.
## Returns: dictionary of GO (key) namespaces (value) (biological_process, molecular_function, cellular_component) according to Gene Ontology's GO.obo file.
##

def getNameSpaces(crowdGOPath):
	nameSpaceDic = defaultdict(str)
	infile = open(crowdGOPath+'/data/nameSpaces.tab')
	for line in infile.readlines():
		go,nameSpace = line.strip().split('\t')
		if not go.startswith('GO:'):
			go = 'GO:'+go
		nameSpaceDic[go] = nameSpace
	return nameSpaceDic

def namespaceGOCount(goCountDic,nameSpaceDic):
        namespaceGoCountDic = defaultdict(int)
        bpCount = 0
        mfCount = 0
        ccCount = 0
        for go,count in goCountDic.items():
                namespace = nameSpaceDic[go]
                if namespace == 'biological_process':
                        bpCount += int(count)
                elif namespace == 'molecular_function':
                        mfCount += int(count)
                elif namespace == 'cellular_component':
                        ccCount += int(count)
        namespaceGoCountDic['cellular_component'] = ccCount
        namespaceGoCountDic['molecular_function'] = mfCount
        namespaceGoCountDic['biological_process'] = bpCount
        return(namespaceGoCountDic)

def calcIC(go,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic):
        nameSpace = nameSpaceDic[go]
        if not nameSpace == 'biological_process' and not nameSpace == 'molecular_function' and not nameSpace == 'cellular_component':
                nameSpace = 'biological_process'
        nameSpaceGOCount = namespaceGoCountDic[nameSpace]
        goCount = goCountDic[go]
        nameSpaceGOCount = namespaceGoCountDic[nameSpace]
        ic = 0
        children = childDic[go]
        for child in children:
                if child in goCountDic:
                        goCount += goCountDic[child]
        if goCount == 0:
                goCount = 1
        ic = -log10(float(goCount)/nameSpaceGOCount)
        return(ic)

def semSim(ic1,ic2):
        icMin = ic1
        if ic1 > ic2:
                icMin = ic2
        semSim = (2*icMin)/(ic1+ic2)
        return(semSim)

##
## Function to cluster GO terms from different methods for the same protein. A cluster contains GO terms which are in each others GO Slim (goSlimDic). Returned is the clusterDic which has a protein as key, and a list of list as values. Each list contains the predictions for each method. In the case of an absent prediction for this cluster for one of the methods, that methods 'prediction' is a 'None' value
## Returns: dictionary with for every protein (key) all of its GO term predictions from all methods which occur in each others GO DAG.
##

def createClusters(predictionDictionary,goParentDic,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic,outDirPath):
        if not os.path.isdir(outDirPath):
                os.makedirs(outDirPath)
        outfileFull = open(outDirPath+'/predictionFeatures.csv','w')
        outfileAbstract = open(outDirPath+'/predictionFeatures_abstract.csv','w')
        methods = list(predictionDictionary.keys())
        methods.sort()
        for i in range(0,len(methods)):
                method1 = methods[i]
                count = 0
                for protein,predictions in predictionDictionary[method1].items(): ## Loop over the predictions for each method
                        for prediction in predictions:
                                go1,score1 = prediction ## Extract the values in the prediction
                                ic1 = calcIC(go1,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic)
                                groupList = [None]*len(methods)
                                groupList[i] = [method1,go1,ic1,score1]
                                for k in range(0,len(methods)):
                                        if not k == i:
                                                method2 = methods[k]
                                                maxSemanticSimilarity = 0
                                                maxSemanticSimilarityGO = 'GO:'
                                                maxSemanticSimilarityScore = 0
                                                maxSemanticSimilarityIC = 0
                                                if protein in predictionDictionary[method2]:
                                                        for prediction in predictionDictionary[method2][protein]: ## Loop over the predictions for the 2nd method
                                                                go2,score2 = prediction ## Extract the values in the prediction
                                                                if go2 in goParentDic[go1]:
                                                                        ic2 = calcIC(go2,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic)
                                                                        semanticSimilarity = semSim(ic1,ic2)
                                                                        if semanticSimilarity >= 0.5:
                                                                            if semanticSimilarity > maxSemanticSimilarity:
                                                                                    maxSemanticSimilarity = semanticSimilarity
                                                                                    maxSemanticSimilarityGO = go2
                                                                                    maxSemanticSimilarityScore = score2
                                                                                    maxSemanticSimilarityIC = ic2
                                                groupList[k] = [method2,maxSemanticSimilarityGO,maxSemanticSimilarityIC,maxSemanticSimilarityScore]
                                semSimList = []
                                toolCount = 0
                                for k in range(0,len(groupList)):
                                        icK = groupList[k][2]
                                        for j in range(0,len(groupList)):
                                                if j > k:
                                                        if not groupList[j][1] == 'GO:':
                                                                icJ = groupList[j][2]
                                                                semanticSimilarityKJ = semSim(icK,icJ)
                                                                semSimList.append(semanticSimilarityKJ)
                                                                toolCount += 1
                                                        else:
                                                                semSimList.append(0)
                                outString = protein+','+go1+','+str(ic1)+','+str(toolCount)
                                outStringAbstract = str(ic1)+','+str(toolCount)
                                for k in range(0,len(groupList)):
                                        method,go,ic,score = groupList[k]
                                        outString += ','+method+','+go+','+str(ic)+','+str(score)
                                        outStringAbstract += ','+str(ic)+','+str(score)
                                for semanticSimilarity in semSimList:
                                        outString += ','+str(semanticSimilarity)
                                        outStringAbstract += ','+str(semanticSimilarity)
                                outfileFull.write(outString+'\n')
                                outfileAbstract.write(outStringAbstract+'\n')
                        count += 1
                        if count%1000 == 0:
                                print(count)
        outfileFull.close()
        outfileAbstract.close()


##
## Function to execute the machine learning algorithm.
## Returns nothing, instead writes the predictions to the output folder given by -o. Predictions are given in two files: a raw file containing all information, and the 'annotation.tab' which is a tabular file containing the protein, GO term, and probability score.
##

def model(modelFilePath,outDirPath):
	predictionsFile_abstract = pd.read_csv(outDirPath+'/predictionFeatures_abstract.csv',header=None) ## Read the abstract predictions
	predictionsFile = open(outDirPath+'/predictionFeatures.csv') ## Read the full predictions
	modelTrained = joblib.load(modelFilePath) ## Loading the model
	print('Predicting on model')
	predictions = modelTrained.predict_proba(predictionsFile_abstract) ## Apply the model to the predictions
	print('Predicted on model, writing output file')
	outFileRaw = open(outDirPath+'/crowdgo.raw.csv','w') ## Write the raw annotations to the temporary folder
	outfile = open(outDirPath+'/crowdgo.tab','w') ## Write the predictions to the user supplied outfile path
	outfile.write('Protein\tGO term\tScore')
	annotDict = defaultdict(dict)
	count = 0
	for line in predictionsFile.readlines():
		ssline = line.strip().split(',')
		protein = ssline[0]
		go = ssline[1]
		score = float(predictions[count][1]) ## For AdaBoost etc
		if not go in annotDict[protein]:
			annotDict[protein][go] = score
		elif float(score) > annotDict[protein][go]:
			annotDict[protein][go] = score
		count += 1
	for protein,gos in annotDict.items():
		for go in gos:
			score = annotDict[protein][go]
			outFileRaw.write('\n'+line.strip()+','+str(score))
			if score >= 0.5:
				outfile.write('\n'+protein+'\t'+go+'\t'+str(score))
	outfile.close()
	outFileRaw.close()

crowdGOPath = os.path.dirname(os.path.abspath(__file__))

start_time = time.time()
inputFilePath,rfFilePath,outDirPath = readArguments()
print('Read input files in: '+str(time.time()-start_time)+' seconds',flush=True)
predictionDictionary = readInput(inputFilePath)
print('Created prediction dictionary in: '+str(time.time()-start_time)+' seconds',flush=True)
goSlimDic,parentDic,childDic = goSlim(crowdGOPath)
print('Created relationship dictionary in: '+str(time.time()-start_time)+' seconds',flush=True)
goCountDic,totGoCounts = getGoCounts(crowdGOPath)
print('Created goCounts in: '+str(time.time()-start_time)+' seconds',flush=True)
nameSpaceDic = getNameSpaces(crowdGOPath)
namespaceGoCountDic = namespaceGOCount(goCountDic,nameSpaceDic)
print('Read namespaces in: '+str(time.time()-start_time)+' seconds',flush=True)
createClusters(predictionDictionary,parentDic,childDic,goCountDic,nameSpaceDic,namespaceGoCountDic,outDirPath)
print('Wrote prediction files for model in: '+str(time.time()-start_time)+' seconds',flush=True)
model(rfFilePath,outDirPath)
print('CrowdGO runtime: '+str(time.time()-start_time)+' seconds',flush=True)
